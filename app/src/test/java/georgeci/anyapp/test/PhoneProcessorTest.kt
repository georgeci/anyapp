@file:Suppress("IllegalIdentifier")

package georgeci.anyapp.test

import georgeci.anyapp.model.PhoneProcessorImpl
import org.funktionale.option.toOption
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class PhoneProcessorTest(
        val input: String,
        val output: String?,
        val validPhone: String
) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "Tests for number {0}")
        fun params() = listOf(
                arrayOf("", null, false),
                arrayOf("+", null, false),
                arrayOf("+7", "RU", false),
                arrayOf("+78", "RU", false),
                arrayOf("+3", null, false),
                arrayOf("+37", null, false),
                arrayOf("+375", "BY", false)
        )
    }

    val p = PhoneProcessorImpl()

    @Test
    fun `validate region`() {
        val result = p.regionByPhone(input)

        Assert.assertThat(result, CoreMatchers.equalTo(output.toOption()))
    }
}