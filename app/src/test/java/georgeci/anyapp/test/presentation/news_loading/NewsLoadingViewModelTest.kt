package georgeci.anyapp.test.presentation.news_loading

import georgeci.anyapp.model.NewsResolution
import georgeci.anyapp.screen.news_loading.NewsLoadingState
import georgeci.anyapp.screen.news_loading.NewsLoadingViewModel
import georgeci.anyapp.test.presentation.news_loading.mock.MockNewsInteractor
import georgeci.anyapp.test.presentation.news_loading.mock.MockNewsLoadingView
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import java.util.Date

class NewsLoadingViewModelTest {
  lateinit var viewModel: NewsLoadingViewModel
  lateinit var view: MockNewsLoadingView
  lateinit var interactor: MockNewsInteractor

  @Before
  fun setup() {
    view = MockNewsLoadingView()
    interactor = MockNewsInteractor()
    viewModel = NewsLoadingViewModel(
            interactor = interactor,
            computation = Schedulers.trampoline(),
            mainThread = Schedulers.trampoline()
    )
  }

  @Test
  fun firstTEst() {
    val t = viewModel.state.test()
    t.assertValues(NewsLoadingState.Syncing)
  }

  @Test
  fun secTEst() {
    val t = viewModel.state.test()
    val date = Date()
    interactor.subject.onNext(NewsResolution.Cache(listOf(), date))

    t.assertValues(NewsLoadingState.Syncing, NewsLoadingState.SyncingWithCache(date))
  }

  @Test
  fun thrdTEst() {
    val t = viewModel.state.test()
    val date = Date()
    interactor.subject.onNext(NewsResolution.Cache(listOf(), date))
    interactor.subject.onNext(NewsResolution.FailWithCache(listOf(), date))

    t.assertValues(
            NewsLoadingState.Syncing,
            NewsLoadingState.SyncingWithCache(date),
            NewsLoadingState.FailedWithCache(date)
    )
  }

  @Test
  fun fourTEst() {
    val t = viewModel.state.test()
    val date = Date()
    interactor.subject.onNext(NewsResolution.Cache(listOf(), date))
    interactor.subject.onNext(NewsResolution.FailWithCache(listOf(), date))
    interactor.subject.onNext(NewsResolution.Success(listOf()))

    t.assertValues(
            NewsLoadingState.Syncing,
            NewsLoadingState.SyncingWithCache(date),
            NewsLoadingState.FailedWithCache(date),
            NewsLoadingState.Synced
    )
  }
}