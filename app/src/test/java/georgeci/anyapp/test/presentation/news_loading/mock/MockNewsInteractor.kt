package georgeci.anyapp.test.presentation.news_loading.mock

import georgeci.anyapp.model.NewsInteractor
import georgeci.anyapp.model.NewsResolution
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class MockNewsInteractor : NewsInteractor {
  val subject = PublishSubject.create<NewsResolution>()

  override fun syncNews() {
  }

  override fun observeNews(): Observable<NewsResolution> {
    return subject
  }
}