package georgeci.anyapp.test.presentation.news_loading.mock

import georgeci.anyapp.screen.news_loading.NewsLoadingState
import georgeci.anyapp.screen.news_loading.NewsLoadingView
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

class MockNewsLoadingView : NewsLoadingView {
  override val stateConsumer = Consumer<NewsLoadingState> {

  }
  override val refresh = PublishSubject.create<Unit>()
}