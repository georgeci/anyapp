package georgeci.anyapp.app

import android.app.Application
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import georgeci.anyapp.di.createCommonDiConfig

class AnyApp : Application(), KodeinAware {
  override val kodein = Kodein {
    import(createCommonDiConfig())
  }
}