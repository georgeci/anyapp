package georgeci.anyapp.api

data class NewsResponse(
        val items: List<NewsEntity>
)