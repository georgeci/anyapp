package georgeci.anyapp.api

import io.reactivex.Observable

interface NewsApi {
  fun loadNews(): Observable<NewsResponse>
}