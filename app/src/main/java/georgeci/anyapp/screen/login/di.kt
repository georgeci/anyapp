package georgeci.anyapp.screen.login

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton

fun LoginActivity.createDiConfig() = Kodein.Module {
  bind<LoginView>() with singleton {
    LoginViewImpl(window.decorView)
  }

  bind<LoginRouter>() with singleton {
    LoginRouterImpl(this@createDiConfig)
  }

  bind<LoginViewModel>() with singleton {
    ViewModelProviders
            .of(this@createDiConfig, instance<ViewModelProvider.Factory>())
            .get(LoginViewModel::class.java)
  }
}