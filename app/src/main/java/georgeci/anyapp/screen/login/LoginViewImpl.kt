package georgeci.anyapp.screen.login

import android.support.design.widget.TextInputLayout
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges
import georgeci.anyapp.R
import io.reactivex.Observable
import io.reactivex.functions.Consumer

class LoginViewImpl(
        view: View,
        phoneInputWatcher: TextWatcher = PhoneNumberFormattingTextWatcher()
) : LoginView {
  private val phone: EditText = view.findViewById(R.id.phone)
  private val phoneInput: TextInputLayout = view.findViewById(R.id.phone_input)
  private val login: Button = view.findViewById(R.id.login)

  init {
    phone.addTextChangedListener(phoneInputWatcher)
  }

  override val loginClickEvents: Observable<Unit> = login.clicks()

  override val loginInputEvents: Observable<String> = phone.textChanges()
          .skipInitialValue()
          .map(CharSequence::toString)

  override val stateConsumer = Consumer<LoginState> {

  }
}