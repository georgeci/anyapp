package georgeci.anyapp.screen.login

import georgeci.anyapp.utils.bindable.Bindable
import io.reactivex.Observable
import io.reactivex.functions.Consumer

interface LoginView : Bindable {
  val loginClickEvents: Observable<Unit>
  val loginInputEvents: Observable<String>
  val stateConsumer: Consumer<LoginState>
}