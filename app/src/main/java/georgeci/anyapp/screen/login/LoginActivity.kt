package georgeci.anyapp.screen.login

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.github.salomonbrys.kodein.KodeinInjector
import com.github.salomonbrys.kodein.android.ActivityInjector
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import georgeci.anyapp.R

class LoginActivity : AppCompatActivity(), ActivityInjector {

  private val view: LoginView by instance()
  private val router: LoginRouter by instance()
  private val viewModel: LoginViewModel by instance()

  override val injector = KodeinInjector()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.login_activity)
    inject(appKodein())
  }

  override fun onResume() {
    super.onResume()
    viewModel.attachView(view)
    viewModel.attachRouter(router)
  }

  override fun onPause() {
    super.onPause()
    viewModel.dettachView()
    viewModel.dettachRouter()
  }

  override fun provideOverridingModule() = createDiConfig()
}