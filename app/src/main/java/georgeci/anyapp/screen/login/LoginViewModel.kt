package georgeci.anyapp.screen.login

import com.jakewharton.rxrelay2.BehaviorRelay
import com.pacoworks.rxcomprehensions.RxComprehensions.doSwitchMap
import georgeci.anyapp.model.PhoneProcessor
import georgeci.anyapp.utils.RxViewModel
import georgeci.anyapp.utils.bindable.BindDelegateImpl
import georgeci.anyapp.utils.just
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.plusAssign

class LoginViewModel(
        val phoneValidator: PhoneProcessor,
        val computation: Scheduler,
        val mainThread: Scheduler
) : RxViewModel() {
  private val viewBindDelegate = BindDelegateImpl<LoginView>()
  private val routerBindDelegate = BindDelegateImpl<LoginRouter>()

  private val loginInputStream = viewBindDelegate.bindableStream { it.loginInputEvents }
  private val loginClickStream = viewBindDelegate.bindableStream { it.loginClickEvents }

  private val stateRelay = BehaviorRelay.createDefault<LoginState>(LoginState.Idle)

  init {
    lifecycleDisposable += doSwitchMap(
            { stateRelay },
            { loginInputStream.subscribeOn(computation) },
            { _, phone ->
              val validate = phoneValidator.validate(phone)
              when (validate) {
                is PhoneProcessor.LoginValidationInfo.Valid -> LoginState.Valid(phone)
                is PhoneProcessor.LoginValidationInfo.InValid -> LoginState.InValid(phone, "")
              }.just()
            })
            .subscribe(stateRelay)

    routerBindDelegate.onBind {
      loginClickStream with bindable.routeToMain
    }

    viewBindDelegate.onBind {
      stateRelay.observeOn(mainThread) with bindable.stateConsumer
    }
  }

  val attachView = viewBindDelegate::attach
  val dettachView = viewBindDelegate::detach
  val attachRouter = routerBindDelegate::attach
  val dettachRouter = routerBindDelegate::detach

}