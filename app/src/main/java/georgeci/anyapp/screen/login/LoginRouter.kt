package georgeci.anyapp.screen.login

import georgeci.anyapp.utils.bindable.Bindable
import io.reactivex.functions.Consumer

interface LoginRouter : Bindable {
  val routeToMain: Consumer<Unit>
}