package georgeci.anyapp.screen.login

sealed class LoginState {
  abstract val phone: String

  object Idle : LoginState() {
    override val phone: String = ""
  }

  data class Valid(
          override val phone: String
  ) : LoginState()

  data class InValid(
          override val phone: String,
          val message: String
  ) : LoginState()
}