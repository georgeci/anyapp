package georgeci.anyapp.screen.news_loading

import java.util.Date

sealed class NewsLoadingState {
  object Syncing : NewsLoadingState()
  data class SyncingWithCache(val date: Date) : NewsLoadingState()
  object Synced : NewsLoadingState()
  object Failed : NewsLoadingState()
  data class FailedWithCache(val date: Date) : NewsLoadingState()
}