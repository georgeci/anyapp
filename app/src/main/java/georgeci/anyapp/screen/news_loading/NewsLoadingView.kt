package georgeci.anyapp.screen.news_loading

import georgeci.anyapp.utils.bindable.Bindable
import io.reactivex.Observable
import io.reactivex.functions.Consumer

interface NewsLoadingView : Bindable {
  val stateConsumer: Consumer<NewsLoadingState>
  val refresh: Observable<Unit>
}