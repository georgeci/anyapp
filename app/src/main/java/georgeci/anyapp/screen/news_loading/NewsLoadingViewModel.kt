package georgeci.anyapp.screen.news_loading

import georgeci.anyapp.model.NewsInteractor
import georgeci.anyapp.model.NewsResolution
import georgeci.anyapp.utils.RxViewModel
import georgeci.anyapp.utils.bindable.BindDelegateImpl
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.subjects.BehaviorSubject

class NewsLoadingViewModel(
        private val interactor: NewsInteractor,
        private val computation: Scheduler,
        private val mainThread: Scheduler
) : RxViewModel() {
  private val viewBindDelegate = BindDelegateImpl<NewsLoadingView>()
  private val refreshStream = viewBindDelegate.bindableStream { it.refresh }

  val state = BehaviorSubject.createDefault<NewsLoadingState>(NewsLoadingState.Syncing)

  init {
    state
            .subscribeOn(computation)
            .switchMap { oldState ->
              when (oldState) {
                is NewsLoadingState.Syncing -> {
                  interactor.syncNews()
                  listenResult()
                }
                is NewsLoadingState.SyncingWithCache -> {
                  interactor.syncNews()
                  listenResult()
                }
                is NewsLoadingState.Failed -> {
                  listenResult()
                }
                is NewsLoadingState.FailedWithCache -> {
                  listenResult()
                }
                is NewsLoadingState.Synced -> {
                  listenResult()
                }
              }
            }
            .subscribe(state)

    viewBindDelegate.onBind {
      state.observeOn(mainThread) with bindable.stateConsumer
    }
  }

  private fun listenResult(): Observable<NewsLoadingState> {
    return interactor.observeNews()
            .map {
              when (it) {
                is NewsResolution.Success -> NewsLoadingState.Synced
                is NewsResolution.Fail -> NewsLoadingState.Failed
                is NewsResolution.Cache -> NewsLoadingState.SyncingWithCache(it.date)
                is NewsResolution.FailWithCache -> NewsLoadingState.FailedWithCache(it.date)
              }
            }
  }
}