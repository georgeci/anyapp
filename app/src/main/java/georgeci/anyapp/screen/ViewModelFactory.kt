package georgeci.anyapp.screen

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import georgeci.anyapp.screen.login.LoginViewModel
import georgeci.anyapp.utils.TAG

class ViewModelFactory(
        private val kodein: Kodein
) : ViewModelProvider.Factory {
  @Suppress("UNCHECKED_CAST")
  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    val vm = when (modelClass) {
      LoginViewModel::class.java -> LoginViewModel(
              phoneValidator = kodein.instance(),
              computation = kodein.instance(TAG.COMPUTATION),
              mainThread = kodein.instance(TAG.MAIN_THREAD)
      )
      else -> error("Not support ViewModel: ${modelClass.simpleName}")
    }
    return vm as T
  }

}