package georgeci.anyapp.utils

import io.reactivex.Observable

inline fun <reified T> T.just() = Observable.just(this)