package georgeci.anyapp.utils.bindable

interface BindDelegate<in T : Bindable> {
  fun attach(t: T)
  fun detach()
}