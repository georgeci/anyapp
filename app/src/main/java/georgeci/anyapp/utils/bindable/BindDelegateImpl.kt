package georgeci.anyapp.utils.bindable

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.rxkotlin.plusAssign

class BindDelegateImpl<T : Bindable> : BindDelegate<T> {
  private val disposable = CompositeDisposable()
  private val bindables = mutableListOf<Pair<T.() -> Observable<*>, Relay<*>>>()
  private val bindsRelay: Relay<T> = PublishRelay.create<T>()

  fun <R> bindableStream(relay: Relay<R> = PublishRelay.create(), map: (T) -> Observable<R>): Observable<R> {
    bindables += map to relay
    return relay
  }

  val binds: Observable<T> = bindsRelay

  override fun attach(t: T) {
    bindables.forEach { (source, target) ->
      @Suppress("UNCHECKED_CAST")
      disposable += source(t)
              .onTerminateDetach()
              .subscribe(target as Consumer<in Any>)
    }
    bindsRelay.accept(t)
  }

  override fun detach() {
    disposable.clear()
  }

  fun onBind(init: Binder<T>.() -> Unit): Disposable {
    return binds
            .subscribe {
              val binder = Binder(it)
              binder.init()
              disposable += binder.disposable
            }
  }

  class Binder<T>(val bindable: T) {
    val disposable = CompositeDisposable()
    infix fun <T> Observable<T>.with(consumer: Consumer<T>) {
      disposable += subscribe(consumer)
    }
  }
}