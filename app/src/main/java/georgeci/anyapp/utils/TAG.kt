package georgeci.anyapp.utils

object TAG {
  val COMPUTATION = "comp"
  val MAIN_THREAD = "main thread"
}