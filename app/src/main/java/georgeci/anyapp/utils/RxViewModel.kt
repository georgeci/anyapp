package georgeci.anyapp.utils

import android.arch.lifecycle.ViewModel
import android.support.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable

abstract class RxViewModel : ViewModel() {
  protected val lifecycleDisposable = CompositeDisposable()

  @CallSuper
  override fun onCleared() {
    super.onCleared()
    lifecycleDisposable.dispose()
  }
}