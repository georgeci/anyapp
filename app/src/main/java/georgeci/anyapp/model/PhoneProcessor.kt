package georgeci.anyapp.model

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import org.funktionale.option.Option
import org.funktionale.option.toOption

interface PhoneProcessor {
  fun regionByPhone(phone: String): Option<String>
  fun validate(phone: String): LoginValidationInfo

  sealed class LoginValidationInfo {
    object Valid : LoginValidationInfo()
    object InValid : LoginValidationInfo()
  }
}

class PhoneProcessorImpl : PhoneProcessor {

  private val emptyRegion = ""
  private val notFoundRegion = "ZZ"
  private val phoneUtil = PhoneNumberUtil.getInstance()

  override fun regionByPhone(phone: String): Option<String> {
    val cleanPhone = phone.replace("+", "")
    (0..Math.min(2, cleanPhone.length - 1))
            .map { i ->
              cleanPhone.slice(0..i).toIntOrNull()?.let {
                phoneUtil.getRegionCodeForCountryCode(it)
              }
            }
            .filter { it != notFoundRegion }
            .forEach { return it.toOption() }
    return Option.None
  }

  override fun validate(phone: String): PhoneProcessor.LoginValidationInfo {
    val parsedPhone = try {
      phoneUtil.parse(phone, emptyRegion)
    } catch (e: NumberParseException) {
      null
    }
    return when (parsedPhone) {
      null -> PhoneProcessor.LoginValidationInfo.InValid
      else -> PhoneProcessor.LoginValidationInfo.Valid
    }
  }
}