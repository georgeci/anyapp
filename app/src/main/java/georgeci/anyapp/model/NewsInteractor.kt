package georgeci.anyapp.model

import io.reactivex.Observable
import java.util.Date

interface NewsInteractor {
  fun syncNews()
  fun observeNews(): Observable<NewsResolution>
}

sealed class NewsResolution {
  data class Cache(val items: List<String>, val date: Date) : NewsResolution()
  data class Success(val items: List<String>) : NewsResolution()
  object Fail : NewsResolution()
  data class FailWithCache(val items: List<String>, val date: Date) : NewsResolution()
}