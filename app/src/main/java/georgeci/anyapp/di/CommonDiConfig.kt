package georgeci.anyapp.di

import android.arch.lifecycle.ViewModelProvider
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import georgeci.anyapp.app.AnyApp
import georgeci.anyapp.model.PhoneProcessor
import georgeci.anyapp.model.PhoneProcessorImpl
import georgeci.anyapp.screen.ViewModelFactory

fun AnyApp.createCommonDiConfig() = Kodein.Module {
  bind<ViewModelProvider.Factory>() with singleton {
    ViewModelFactory(kodein = instance())
  }

  bind<PhoneProcessor>() with singleton {
    PhoneProcessorImpl()
  }
}